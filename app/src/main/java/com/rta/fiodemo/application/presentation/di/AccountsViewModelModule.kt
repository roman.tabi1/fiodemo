package com.rta.fiodemo.application.presentation.di

import androidx.lifecycle.ViewModel
import com.aheaditec.architecture.presentation.presenter.di.BaseViewModelModule
import com.aheaditec.architecture.presentation.presenter.di.ViewModelKey
import com.rta.presentation.accounts.presenter.AccountsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AccountsViewModelModule : BaseViewModelModule() {

    @Binds
    @IntoMap
    @ViewModelKey(AccountsViewModel::class)
    abstract fun bindsAccountsViewModel(AccountsViewModel: AccountsViewModel): ViewModel
}
