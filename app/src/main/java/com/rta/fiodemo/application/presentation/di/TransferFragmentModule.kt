package com.rta.fiodemo.application.presentation.di

import com.rta.presentation.transfer.view.TransferFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [TransferFragmentModule.AbstractTransferFragmentModule::class])
class TransferFragmentModule {

    @Module
    interface AbstractTransferFragmentModule {
        @ContributesAndroidInjector
        fun contributeTransferFragment(): TransferFragment
    }
}