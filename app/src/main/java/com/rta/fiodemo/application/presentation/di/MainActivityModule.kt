package com.rta.fiodemo.application.presentation.di

import com.rta.presentation.main.view.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [MainActivityModule.AbstractMainActivityModule::class])
class MainActivityModule {

    @Module
    interface AbstractMainActivityModule {
        @ContributesAndroidInjector
        fun contributeMainActivity(): MainActivity
    }
}