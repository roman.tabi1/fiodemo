package com.rta.fiodemo.application.di

import com.rta.fiodemo.application.presentation.di.AccountsFragmentModule
import com.rta.fiodemo.application.presentation.di.DetailFragmentModule
import com.rta.fiodemo.application.presentation.di.MainActivityModule
import com.rta.fiodemo.application.presentation.di.TransferFragmentModule
import dagger.Module

@Module(
    includes = [
        MainActivityModule::class,
        AccountsFragmentModule::class,
        DetailFragmentModule::class,
        TransferFragmentModule::class,
    ]
)
abstract class FragmentModules
