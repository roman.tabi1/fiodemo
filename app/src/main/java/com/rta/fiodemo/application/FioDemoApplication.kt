package com.rta.fiodemo.application

import com.aheaditec.architecture.application.BaseApplication
import com.aheaditec.architecture.di.ApplicationComponent
import com.rta.fiodemo.application.di.DaggerFioDemoApplicationComponent
import timber.log.Timber

class FioDemoApplication : BaseApplication() {

    override fun onCreateComponent(): ApplicationComponent = DaggerFioDemoApplicationComponent
        .builder()
        .application(this)
        .build()

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())
    }
}