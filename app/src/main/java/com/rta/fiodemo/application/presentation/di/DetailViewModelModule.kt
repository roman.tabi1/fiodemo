package com.rta.fiodemo.application.presentation.di

import androidx.lifecycle.ViewModel
import com.aheaditec.architecture.presentation.presenter.di.BaseViewModelModule
import com.aheaditec.architecture.presentation.presenter.di.ViewModelKey
import com.rta.presentation.detail.presenter.DetailViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DetailViewModelModule : BaseViewModelModule() {

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    abstract fun bindsDetailViewModel(DetailViewModel: DetailViewModel): ViewModel
}

