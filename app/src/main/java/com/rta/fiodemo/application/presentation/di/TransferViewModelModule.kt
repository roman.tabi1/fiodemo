package com.rta.fiodemo.application.presentation.di

import androidx.lifecycle.ViewModel
import com.aheaditec.architecture.presentation.presenter.di.BaseViewModelModule
import com.aheaditec.architecture.presentation.presenter.di.ViewModelKey
import com.rta.presentation.transfer.presenter.TransferViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class TransferViewModelModule : BaseViewModelModule() {

    @Binds
    @IntoMap
    @ViewModelKey(TransferViewModel::class)
    abstract fun bindsTransferViewModel(transferViewModel: TransferViewModel): ViewModel
}