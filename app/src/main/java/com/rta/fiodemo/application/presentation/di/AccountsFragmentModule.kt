package com.rta.fiodemo.application.presentation.di

import com.rta.presentation.accounts.view.AccountsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [AccountsFragmentModule.AbstractAccountsFragmentModule::class])
class AccountsFragmentModule {

    @Module
    interface AbstractAccountsFragmentModule {
        @ContributesAndroidInjector
        fun contributeAccountsFragment(): AccountsFragment
    }
}

