package com.rta.fiodemo.application.di

import dagger.Module
import dagger.Provides
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Named

@Module
class DateModule {

    @Provides
    @Named("DateTimeFormat")
    fun provideDateTimeFormat(): SimpleDateFormat =
        SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault())
}
