package com.rta.fiodemo.application.di

import android.app.Application
import com.aheaditec.architecture.di.ApplicationComponent
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        FragmentModules::class,
        ViewModelModules::class,
        RepositoryModule::class,
        StorageModule::class,
        DateModule::class,
        AndroidSupportInjectionModule::class,
    ]
)
interface FioDemoApplicationComponent : ApplicationComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): FioDemoApplicationComponent
    }
}