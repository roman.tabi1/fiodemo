package com.rta.fiodemo.application.presentation.di

import com.rta.presentation.detail.view.DetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [DetailFragmentModule.AbstractDetailFragmentModule::class])
class DetailFragmentModule {

    @Module
    interface AbstractDetailFragmentModule {
        @ContributesAndroidInjector
        fun contributeDetailFragment(): DetailFragment
    }
}
