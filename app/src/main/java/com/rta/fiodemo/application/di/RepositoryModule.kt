package com.rta.fiodemo.application.di

import com.rta.data.account.AccountRepositoryImpl
import com.rta.data.transfer.TransferRepositoryImpl
import com.rta.domain.account.AccountRepository
import com.rta.domain.transfer.TransferRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {

    @Singleton
    @Binds
    abstract fun provideAccountRepository(accountRepository: AccountRepositoryImpl): AccountRepository

    @Singleton
    @Binds
    abstract fun provideTransferRepository(transferRepository: TransferRepositoryImpl): TransferRepository
}