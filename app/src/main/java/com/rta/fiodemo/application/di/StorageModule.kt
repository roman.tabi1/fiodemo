package com.rta.fiodemo.application.di

import android.app.Application
import android.content.Context
import android.os.Environment
import com.rta.data.account.AccountRepositoryImpl
import com.rta.domain.account.AccountRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import java.io.File
import javax.inject.Named
import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Named("FilesDir")
    fun provideFilesDir(application: Application): File = application.filesDir
}
