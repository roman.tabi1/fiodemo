package com.rta.fiodemo.application.di

import com.rta.fiodemo.application.presentation.di.AccountsViewModelModule
import com.rta.fiodemo.application.presentation.di.DetailFragmentModule
import com.rta.fiodemo.application.presentation.di.DetailViewModelModule
import com.rta.fiodemo.application.presentation.di.MainViewModelModule
import com.rta.fiodemo.application.presentation.di.TransferFragmentModule
import com.rta.fiodemo.application.presentation.di.TransferViewModelModule
import dagger.Module

@Module(
    includes = [
        MainViewModelModule::class,
        AccountsViewModelModule::class,
        DetailViewModelModule::class,
        TransferViewModelModule::class,
    ]
)
abstract class ViewModelModules
