package com.rta.fiodemo.application.presentation.di

import androidx.lifecycle.ViewModel
import com.aheaditec.architecture.presentation.presenter.di.BaseViewModelModule
import com.aheaditec.architecture.presentation.presenter.di.ViewModelKey
import com.rta.presentation.main.presenter.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MainViewModelModule : BaseViewModelModule() {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindsMainViewModel(mainViewModel: MainViewModel): ViewModel
}