package com.rta.presentation.transfer.view

import com.rta.presentation.BaseViewState

class TransferViewState : BaseViewState("TransferViewState") {

    override val keys: Array<ViewStateAttribute>
        get() = arrayOf(
            Attribute.Account,
            Attribute.Failures,
            Attribute.Transfer,
        )

    sealed class Attribute(key: String) : ViewStateAttribute(key) {
        object Account : Attribute("Account")
        object Failures : Attribute("Failures")
        object Transfer : Attribute("Transfer")
    }

    class Provider : BaseViewStateProvider<TransferViewState>() {
        override val viewState: TransferViewState = TransferViewState()
    }
}