package com.rta.presentation.transfer.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rta.presentation.R
import com.rta.presentation.detail.entity.UiTransfer
import kotlinx.android.synthetic.main.item_transfer.view.*
import javax.inject.Inject

class TransferAdapter @Inject constructor() :
    ListAdapter<UiTransfer, TransferAdapter.ViewHolder>(UiTransfersDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_transfer, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(uiTransfer: UiTransfer) {
            val placeholder = view.context.getString(R.string.placeholder_balance)
            val prettyBalance = String.format(placeholder, uiTransfer.amount, uiTransfer.currency)
            view.text_view_amount.text = prettyBalance
            view.text_view_date.text = uiTransfer.dateTime
            view.text_view_counter_account.text = uiTransfer.counterAccount
        }
    }

    private class UiTransfersDiffCallback : DiffUtil.ItemCallback<UiTransfer>() {

        override fun areItemsTheSame(oldItem: UiTransfer, newItem: UiTransfer): Boolean =
            oldItem === newItem

        override fun areContentsTheSame(oldItem: UiTransfer, newItem: UiTransfer): Boolean =
            oldItem == newItem
    }
}