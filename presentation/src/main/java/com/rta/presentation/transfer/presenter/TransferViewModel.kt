package com.rta.presentation.transfer.presenter

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.aheaditec.architecture.presentation.presenter.BaseViewModel
import com.aheaditec.functional.onLeft
import com.rta.domain.account.entity.Account
import com.rta.domain.transfer.CreateTransfer
import com.rta.domain.transfer.SaveTransfer

import com.rta.presentation.detail.view.DetailViewState
import com.rta.presentation.transfer.view.TransferFragmentArgs
import com.rta.presentation.transfer.view.TransferViewState
import com.rta.presentation.transfer.view.UiTransferFailure
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import timber.log.Timber
import java.math.BigDecimal
import java.math.RoundingMode
import javax.inject.Inject

class TransferViewModel @Inject constructor(
    private val createTransfer: CreateTransfer,
    private val saveTransfer: SaveTransfer,
) : BaseViewModel() {

    private val viewStateProvider: TransferViewState.Provider = TransferViewState.Provider()
    val viewState: LiveData<TransferViewState> = viewStateProvider.data

    private lateinit var account: Account

    override fun onCreate(arguments: Bundle?, savedInstanceState: Bundle?) {
        super.onCreate(arguments, savedInstanceState)

        checkNotNull(arguments) { Timber.e("You have not passed the account!") }
        account = TransferFragmentArgs.fromBundle(arguments).account

        viewStateProvider.update(TransferViewState.Attribute.Account to account)
    }

    fun onSubmitTransferClicked(number: String?, amount: String?) = viewModelScope.launch(IO) {
        val failures = mutableListOf<UiTransferFailure>()

        number ?: failures.add(UiTransferFailure.MissingCounterAccountNumber)
        amount ?: failures.add(UiTransferFailure.MissingAmount)

        if (failures.isNotEmpty()) {
            viewStateProvider.update(TransferViewState.Attribute.Failures to failures)
            return@launch
        }

        checkNotNull(number)
        checkNotNull(amount)

        val bigDecimalAmount = BigDecimal(amount).setScale(2, RoundingMode.HALF_UP)
        val transfer = createTransfer(account, number.toLong(), bigDecimalAmount).onLeft {
            failures.add(UiTransferFailure.InsufficientFunds)
            viewStateProvider.update(TransferViewState.Attribute.Failures to failures)
            return@launch
        }

        saveTransfer(transfer)
        viewStateProvider.update(TransferViewState.Attribute.Transfer to transfer)
    }
}