package com.rta.presentation.transfer.view

import android.os.Bundle
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import com.aheaditec.architecture.presentation.view.ViewModelBaseFragment
import com.rta.domain.account.entity.Account
import com.rta.domain.transfer.entity.Transfer
import com.rta.presentation.R
import com.rta.presentation.transfer.presenter.TransferViewModel
import kotlinx.android.synthetic.main.fragment_transfer.*

class TransferFragment : ViewModelBaseFragment<TransferViewModel>(R.layout.fragment_transfer) {

    override val viewModelClass: Class<TransferViewModel>
        get() = TransferViewModel::class.java

    override fun onInitializeView() {
        super.onInitializeView()

        button_submit_transfer.setOnClickListener {
            val number = edit_text_counter_account_number.text?.toString()?.takeIf { it.isNotBlank() }
            val amount = edit_text_amount.text?.toString()?.takeIf { it.isNotBlank() }

            viewModel.onSubmitTransferClicked(number, amount)
        }
    }

    override fun onSubscribe() {
        super.onSubscribe()

        viewModel.viewState.observe(viewLifecycleOwner, ::updateUi)
    }

    private fun updateUi(viewState: TransferViewState) {
        viewState.get<Transfer?>(TransferViewState.Attribute.Transfer)?.let(::handleTransfer)
        viewState.get<Account?>(TransferViewState.Attribute.Account)?.let(::showAccount)
        viewState.get<List<UiTransferFailure>?>(TransferViewState.Attribute.Failures)
            .let(::handleFailure)
    }

    private fun handleTransfer(transfer: Transfer) {
        val resultBundle = Bundle().apply { putSerializable(KEY_TRANSFER, transfer) }
        setFragmentResult(KEY_TRANSFER_RESULT, resultBundle)
        findNavController().popBackStack()
    }

    private fun showAccount(account: Account) = with(account) {
        val placeholder = getString(R.string.placeholder_balance)
        val prettyBalance = String.format(placeholder, balance.toString(), currency)
        text_view_balance.text = prettyBalance
        text_view_name.text = name
        text_view_number.text = number.toString()
    }

    private fun handleFailure(failures: List<UiTransferFailure>?) {
        failures
            ?.let {
                handleAmountFailure(it)
                handleNumberFailure(it)
            }
            ?: let {
                input_layout_amount.error = null
                input_layout_counter_account.error = null
            }
    }

    private fun handleNumberFailure(failures: List<UiTransferFailure>) {
        if (failures.contains(UiTransferFailure.MissingCounterAccountNumber)) {
            input_layout_counter_account.error = getString(R.string.number_required)
        } else {
            input_layout_counter_account.error = null
        }
    }

    private fun handleAmountFailure(failures: List<UiTransferFailure>) {
        if (failures.contains(UiTransferFailure.InsufficientFunds)) {
            input_layout_amount.error = getString(R.string.insufficient_funds)
        }
        if (failures.contains(UiTransferFailure.MissingAmount)) {
            input_layout_amount.error = getString(R.string.amount_required)
        }
        if (!failures.contains(UiTransferFailure.InsufficientFunds) &&
            !failures.contains(UiTransferFailure.MissingAmount)
        ) {
            input_layout_amount.error = null
        }
    }

    companion object {
        private const val KEY_TRANSFER_RESULT = "transfer_result"
        private const val KEY_TRANSFER = "transfer"
    }
}