package com.rta.presentation.transfer.view

sealed class UiTransferFailure {
    object MissingCounterAccountNumber : UiTransferFailure()
    object MissingAmount : UiTransferFailure()
    object InsufficientFunds : UiTransferFailure()
}