package com.rta.presentation.accounts.presenter

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.aheaditec.architecture.domain.interactor.None
import com.aheaditec.architecture.presentation.presenter.BaseViewModel
import com.aheaditec.functional.onLeft
import com.rta.domain.account.AccountRepository
import com.rta.domain.account.GetAccounts
import com.rta.domain.account.entity.Account
import com.rta.presentation.accounts.view.AccountsViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class AccountsViewModel @Inject constructor(
    private val getAccounts: GetAccounts,
) : BaseViewModel() {

    private val viewStateProvider: AccountsViewState.Provider = AccountsViewState.Provider()
    val viewState: LiveData<AccountsViewState> = viewStateProvider.data

    override fun onCreate(arguments: Bundle?, savedInstanceState: Bundle?) {
        super.onCreate(arguments, savedInstanceState)

        viewModelScope.launch(Dispatchers.IO) {
            val accounts = getAccounts(None).onLeft { return@launch handleAccountsError(it.l) }
            viewStateProvider.update(AccountsViewState.Attribute.Accounts to accounts)
        }
    }

    private fun handleAccountsError(failure: AccountRepository.AccountFailure) {
        viewStateProvider.update(AccountsViewState.Attribute.Failure to failure)
    }

    fun onAccountClicked(account: Account) {

    }
}