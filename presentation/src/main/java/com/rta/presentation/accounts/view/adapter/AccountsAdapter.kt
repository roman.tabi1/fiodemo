package com.rta.presentation.accounts.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rta.domain.account.entity.Account
import com.rta.presentation.R
import kotlinx.android.synthetic.main.item_account.view.*
import javax.inject.Inject

class AccountsAdapter @Inject constructor() :
    ListAdapter<Account, AccountsAdapter.ViewHolder>(AccountsDiffCallback()) {

    var clickListener: (Account) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_account, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

    class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(account: Account, clickListener: (Account) -> Unit) = with(account) {
            val placeholder = view.context.getString(R.string.placeholder_balance)
            val prettyBalance = String.format(placeholder, balance.toString(), currency)
            view.text_view_balance.text = prettyBalance
            view.text_view_name.text = name

            view.setOnClickListener { clickListener(this) }
        }
    }

    private class AccountsDiffCallback : DiffUtil.ItemCallback<Account>() {

        override fun areItemsTheSame(oldItem: Account, newItem: Account): Boolean =
            oldItem === newItem

        override fun areContentsTheSame(oldItem: Account, newItem: Account): Boolean =
            oldItem == newItem
    }
}