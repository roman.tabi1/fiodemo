package com.rta.presentation.accounts.view.navigation

import androidx.navigation.NavController
import com.rta.domain.account.entity.Account
import com.rta.presentation.accounts.view.AccountsFragmentDirections
import javax.inject.Inject

class AccountsNavigator @Inject constructor() {

    private var navController: NavController? = null

    fun init(navController: NavController) {
        this.navController = navController
    }

    fun navigateToDetail(account: Account) {
        val direction = AccountsFragmentDirections.actionAccountsFragmentToDetailFragment(account)
        navController?.navigate(direction)
    }
}