package com.rta.presentation.accounts.view

import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import com.aheaditec.architecture.presentation.view.ViewModelBaseFragment
import com.rta.domain.account.AccountRepository
import com.rta.domain.account.entity.Account
import com.rta.presentation.R
import com.rta.presentation.accounts.presenter.AccountsViewModel
import com.rta.presentation.accounts.view.adapter.AccountsAdapter
import com.rta.presentation.accounts.view.AccountsViewState.Attribute.*
import com.rta.presentation.accounts.view.navigation.AccountsNavigator
import kotlinx.android.synthetic.main.fragment_accounts.*
import javax.inject.Inject

class AccountsFragment : ViewModelBaseFragment<AccountsViewModel>(R.layout.fragment_accounts) {

    override val viewModelClass: Class<AccountsViewModel>
        get() = AccountsViewModel::class.java

    @Inject
    lateinit var adapter: AccountsAdapter

    @Inject
    lateinit var navigator: AccountsNavigator

    override fun onInitializeView() {
        super.onInitializeView()

        navigator.init(findNavController())

        adapter.clickListener = navigator::navigateToDetail
        recycler_view_accounts.adapter = adapter
    }

    override fun onSubscribe() {
        super.onSubscribe()

        viewModel.viewState.observe(viewLifecycleOwner, ::updateUi)
    }

    private fun updateUi(viewState: AccountsViewState) {
        viewState.get<List<Account>?>(Accounts)?.let(::showAccounts)
        viewState.get<AccountRepository.AccountFailure?>(Failure)?.let(::showFailure)
    }

    private fun showAccounts(accounts: List<Account>) {
        recycler_view_accounts.isVisible = true
        adapter.submitList(accounts)

        text_view_failure.isVisible = false
    }

    private fun showFailure(failure: AccountRepository.AccountFailure) {
        recycler_view_accounts.isVisible = false

        // TODO: Display error messages based on error type
        text_view_failure.isVisible = true
        text_view_failure.text = getString(R.string.error)

    }
}