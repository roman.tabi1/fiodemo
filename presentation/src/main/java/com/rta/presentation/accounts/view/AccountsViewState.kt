package com.rta.presentation.accounts.view

import com.rta.presentation.BaseViewState

class AccountsViewState : BaseViewState("AccountsViewState") {

    override val keys: Array<ViewStateAttribute>
        get() = arrayOf(
            Attribute.Accounts,
            Attribute.Failure,
        )

    sealed class Attribute(key: String) : ViewStateAttribute(key) {
        object Accounts : Attribute("Accounts")
        object Failure : Attribute("Failure")
    }

    class Provider : BaseViewStateProvider<AccountsViewState>() {
        override val viewState: AccountsViewState = AccountsViewState()
    }
}