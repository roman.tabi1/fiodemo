package com.rta.presentation

import androidx.lifecycle.MutableLiveData
import timber.log.Timber

abstract class BaseViewState(val name: String = "") {

    protected val map: MutableMap<ViewStateAttribute, Any?> = mutableMapOf()

    abstract val keys: Array<ViewStateAttribute>

    abstract class ViewStateAttribute(val key: String) {
        override fun toString(): String = key
    }

    fun <T> get(attribute: ViewStateAttribute): T {
        if (!keys.contains(attribute)) {
            throw IllegalArgumentException("-- ViewState ${this.javaClass.simpleName} does not support key $attribute.")
        }

        val value = map[attribute]
        return value as T
    }

    protected fun put(vararg data: Pair<ViewStateAttribute, Any?>) {
        data.forEach {
            if (!keys.contains(it.first)) {
                Timber.e("-- Ignoring key ${it.first}")
                return
            }
            Timber.d("-- Update VS: ${it.first.javaClass.simpleName} from ${map[it.first]} to ${it.second}")
            map[it.first] = it.second
        }
    }

    override fun toString(): String {
        return "$name[${this.hashCode()}]: $map"
    }

    abstract class BaseViewStateProvider<VS : BaseViewState> {

        val data: MutableLiveData<VS> = MutableLiveData()

        // Do NOT implement custom getter:
        // override val viewState: MyViewState
        //      get() = MyViewState()
        // That will create a new instance every time you access the VS
        // Correct implementation is assignment
        // override val viewState: MyViewState = MyViewState()
        protected abstract val viewState: VS

        fun update(vararg values: Pair<ViewStateAttribute, Any?>) {
            viewState.put(*values)

            data.postValue(viewState)

            Timber.d("-- ViewState after update: $viewState")
        }
    }
}
