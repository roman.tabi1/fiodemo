package com.rta.presentation.main.view

import com.aheaditec.architecture.presentation.view.ViewModelBaseActivity
import com.rta.presentation.R
import com.rta.presentation.main.presenter.MainViewModel

class MainActivity: ViewModelBaseActivity<MainViewModel>(R.layout.activity_main) {

    override val viewModelClass: Class<MainViewModel>
        get() = MainViewModel::class.java
}