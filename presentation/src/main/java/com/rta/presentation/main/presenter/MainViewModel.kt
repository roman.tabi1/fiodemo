package com.rta.presentation.main.presenter

import com.aheaditec.architecture.presentation.presenter.BaseViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor() : BaseViewModel()