package com.rta.presentation.detail.view

import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import com.aheaditec.architecture.presentation.view.ViewModelBaseFragment
import com.rta.domain.account.entity.Account
import com.rta.presentation.R
import com.rta.presentation.detail.entity.UiTransfer
import com.rta.presentation.detail.navigation.DetailNavigator
import com.rta.presentation.detail.presenter.DetailViewModel
import com.rta.presentation.transfer.adapter.TransferAdapter
import kotlinx.android.synthetic.main.fragment_detail.*
import timber.log.Timber
import javax.inject.Inject

class DetailFragment : ViewModelBaseFragment<DetailViewModel>(R.layout.fragment_detail) {

    override val viewModelClass: Class<DetailViewModel>
        get() = DetailViewModel::class.java

    @Inject
    lateinit var detailNavigator: DetailNavigator

    @Inject
    lateinit var adapter: TransferAdapter

    override fun onInitializeView() {
        super.onInitializeView()

        detailNavigator.init(findNavController())

        recycler_view_transfers.adapter = adapter

        setFragmentResultListener(KEY_TRANSFER_RESULT) { _, bundle ->
            val transfer = checkNotNull(bundle.getSerializable(KEY_TRANSFER))
            Timber.d("-- Transfer created: $transfer")
            viewModel.onTransferCreated()
        }

        button_new_transfer.setOnClickListener {
            detailNavigator.navigateToTransfer(
                viewModel.onNewTransferClicked() ?: return@setOnClickListener
            )
        }
    }

    override fun onSubscribe() {
        super.onSubscribe()

        viewModel.viewState.observe(viewLifecycleOwner, ::updateUi)
    }

    private fun updateUi(viewState: DetailViewState) {
        viewState.get<Account?>(DetailViewState.Attribute.Account)?.let(::showAccount)
        viewState.get<List<UiTransfer>?>(DetailViewState.Attribute.Transfers)?.let(::showTransfers)
    }

    private fun showTransfers(transfers: List<UiTransfer>) {
        adapter.submitList(transfers)
    }

    private fun showAccount(account: Account) = with(account) {
        val placeholder = getString(R.string.placeholder_balance)
        val prettyBalance = String.format(placeholder, balance.toString(), currency)
        text_view_balance.text = prettyBalance
        text_view_name.text = name
        text_view_number.text = number.toString()
    }

    companion object {
        private const val KEY_TRANSFER_RESULT = "transfer_result"
        private const val KEY_TRANSFER = "transfer"
    }
}