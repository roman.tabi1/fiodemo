package com.rta.presentation.detail.presenter

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.aheaditec.architecture.presentation.presenter.BaseViewModel
import com.rta.domain.account.entity.Account
import com.rta.domain.transfer.GetTransfersForAccount
import com.rta.presentation.detail.mapper.UiTransferMapper
import com.rta.presentation.detail.view.DetailFragmentArgs
import com.rta.presentation.detail.view.DetailViewState
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class DetailViewModel @Inject constructor(
    private val getTransfersForAccount: GetTransfersForAccount,
    private val mapper: UiTransferMapper,
) : BaseViewModel() {

    private val viewStateProvider: DetailViewState.Provider = DetailViewState.Provider()
    val viewState: LiveData<DetailViewState> = viewStateProvider.data

    private var account: Account? = null

    override fun onCreate(arguments: Bundle?, savedInstanceState: Bundle?) {
        super.onCreate(arguments, savedInstanceState)

        checkNotNull(arguments) { Timber.e("You have not passed the account!") }
        account = DetailFragmentArgs.fromBundle(arguments).account

        viewStateProvider.update(DetailViewState.Attribute.Account to account)
        viewModelScope.launch(IO) {
            showTransfers()
        }
    }

    fun onNewTransferClicked(): Account? = account

    fun onTransferCreated() = viewModelScope.launch(IO) {
        showTransfers()
    }

    private suspend fun showTransfers() {
        val transfers = getTransfersForAccount(account)
            .mapNotNull { account?.let { account -> mapper.mapForAccount(account, it) } }
        viewStateProvider.update(DetailViewState.Attribute.Transfers to transfers)
    }
}