package com.rta.presentation.detail.navigation

import androidx.navigation.NavController
import com.rta.domain.account.entity.Account
import com.rta.presentation.detail.view.DetailFragmentDirections
import javax.inject.Inject

class DetailNavigator @Inject constructor() {

    private var navController: NavController? = null

    fun init(navController: NavController) {
        this.navController = navController
    }

    fun navigateToTransfer(account: Account) {
        val direction = DetailFragmentDirections.actionDetailFragmentToTransferFragment(account)
        navController?.navigate(direction)
    }
}