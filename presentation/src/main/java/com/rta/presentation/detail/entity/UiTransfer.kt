package com.rta.presentation.detail.entity

data class UiTransfer(
    val dateTime: String,
    val counterAccount: String,
    val amount: String,
    val currency: String,
)