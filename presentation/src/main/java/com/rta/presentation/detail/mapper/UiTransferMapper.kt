package com.rta.presentation.detail.mapper

import com.rta.domain.account.entity.Account
import com.rta.domain.transfer.entity.Transfer
import com.rta.presentation.detail.entity.UiTransfer
import java.math.BigDecimal
import java.text.SimpleDateFormat
import javax.inject.Inject
import javax.inject.Named

class UiTransferMapper @Inject constructor(
    @param:Named("DateTimeFormat") private val dateTimeFormat: SimpleDateFormat,
) {

    fun mapForAccount(account: Account, transfer: Transfer): UiTransfer = with(transfer) {
        val counterAccount = counterAccountNumber.takeIf { it != account.number } ?: account.number
        val isOutGoing = counterAccount == account.number
        val finalAmount = amount.takeIf { isOutGoing } ?: amount.multiply(BigDecimal(-1))

        UiTransfer(
            dateTimeFormat.format(timestamp),
            counterAccount.toString(),
            finalAmount.toString(),
            currency,
        )
    }
}