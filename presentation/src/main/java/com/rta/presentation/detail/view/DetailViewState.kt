package com.rta.presentation.detail.view

import com.rta.presentation.BaseViewState

class DetailViewState : BaseViewState("DetailViewState") {

    override val keys: Array<ViewStateAttribute>
        get() = arrayOf(
            Attribute.Account,
            Attribute.Transfers,
        )

    sealed class Attribute(key: String) : ViewStateAttribute(key) {
        object Account : Attribute("Account")
        object Transfers : Attribute("Transfers")
    }

    class Provider : BaseViewStateProvider<DetailViewState>() {
        override val viewState: DetailViewState = DetailViewState()
    }
}