package com.rta.data.account.response

import com.rta.domain.account.entity.Account
import kotlinx.serialization.Serializable

@Serializable
data class GetAccountsResponse(val accounts: List<Account>)