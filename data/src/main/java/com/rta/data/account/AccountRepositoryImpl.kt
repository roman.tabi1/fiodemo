package com.rta.data.account

import com.aheaditec.functional.Either
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.rta.data.account.response.GetAccountsResponse
import com.rta.data.retrofit.AccountRestApi
import com.rta.domain.account.AccountRepository
import com.rta.domain.account.entity.Account
import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withTimeout
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Converter
import retrofit2.Response
import retrofit2.Retrofit
import java.net.Proxy
import javax.inject.Inject
import kotlin.coroutines.resume

class AccountRepositoryImpl @Inject constructor() : AccountRepository {

    private val converter: Converter.Factory =
        Json.asConverterFactory(MediaType.get("application/json"))

    private val httpClient: OkHttpClient = OkHttpClient.Builder()
        .proxy(Proxy.NO_PROXY)
        .build()

    private val restApi: AccountRestApi = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(converter)
        .client(httpClient)
        .build()
        .create(AccountRestApi::class.java)

    override suspend fun getAccounts(timeout: Long): Either<AccountRepository.AccountFailure, List<Account>> =
        try {
            withTimeout(timeout) {
                suspendCancellableCoroutine { continuation ->
                    restApi.getTestAccounts()
                        .enqueue(RetrofitCallbacks(continuation) { Either.Right(it.accounts) })
                }
            }
        } catch (timeout: TimeoutCancellationException) {
            Either.Left(AccountRepository.AccountFailure.Timeout)
        } catch (exception: Exception) {
            exception.printStackTrace()
            Either.Left(AccountRepository.AccountFailure.Error(exception.message ?: ""))
        }

    private class RetrofitCallbacks(
        private val continuation: CancellableContinuation<Either<AccountRepository.AccountFailure, List<Account>>>,
        private val onResponse: (GetAccountsResponse) -> Either<AccountRepository.AccountFailure, List<Account>>
    ) : Callback<GetAccountsResponse> {

        override fun onResponse(
            call: Call<GetAccountsResponse>,
            response: Response<GetAccountsResponse>
        ) {
            response.body()?.let {
                continuation.resume(onResponse(it))
            } ?: continuation.resume(Either.Left(AccountRepository.AccountFailure.NoData))
        }

        override fun onFailure(call: Call<GetAccountsResponse>, t: Throwable) {
            t.printStackTrace()
            val message = t.message ?: ""
            continuation.resume(Either.Left(AccountRepository.AccountFailure.DataError(message)))
        }
    }

    companion object {
        private const val BASE_URL = "http://kali.fio.cz/"
    }
}