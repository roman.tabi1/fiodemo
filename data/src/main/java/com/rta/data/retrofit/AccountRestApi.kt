package com.rta.data.retrofit

import com.rta.data.account.response.GetAccountsResponse
import retrofit2.Call
import retrofit2.http.GET

internal interface AccountRestApi {

    @GET("test/accounts.json")
    fun getTestAccounts(): Call<GetAccountsResponse>
}