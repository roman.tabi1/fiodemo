package com.rta.data.transfer

import com.rta.domain.transfer.TransferRepository
import com.rta.domain.transfer.entity.Transfer
import kotlinx.serialization.json.Json
import java.io.File
import javax.inject.Inject
import javax.inject.Named

class TransferRepositoryImpl @Inject constructor(
    @param:Named("FilesDir") private val filesDir: File,
) : TransferRepository {

    override suspend fun getTransfers(): List<Transfer> =
        File(filesDir, TRANSFERS_FILE_NAME)
            .takeIf { it.exists() }
            ?.readLines()
            ?.map { Json.decodeFromString(Transfer.serializer(), it) }
            ?: emptyList()

    override suspend fun saveTransfer(transfer: Transfer) {
        val file = File(filesDir, TRANSFERS_FILE_NAME)
        val data = Json.encodeToString(Transfer.serializer(), transfer)
        file.appendText("$data\n")
    }

    companion object {
        private const val TRANSFERS_FILE_NAME = "transfers"
    }
}