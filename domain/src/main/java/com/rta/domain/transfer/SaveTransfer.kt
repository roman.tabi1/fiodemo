package com.rta.domain.transfer

import com.aheaditec.architecture.domain.interactor.BaseUseCase
import com.aheaditec.architecture.domain.interactor.None
import com.aheaditec.functional.Either
import com.rta.domain.account.entity.Account
import com.rta.domain.transfer.entity.Transfer
import javax.inject.Inject

class SaveTransfer @Inject constructor(private val transferRepository: TransferRepository) :
    BaseUseCase<Transfer, Unit>() {

    override suspend fun execute(input: Transfer) = transferRepository.saveTransfer(input)
}