package com.rta.domain.transfer.entity

import com.rta.domain.account.entity.Account
import com.rta.domain.serializer.BigDecimalSerializer
import kotlinx.serialization.Serializable
import java.math.BigDecimal

@Serializable
data class Transfer(
    val timestamp: Long,
    val sourceAccount: Account,
    val counterAccountNumber: Long,
    val currency: String,
    @Serializable(BigDecimalSerializer::class) val amount: BigDecimal,
): java.io.Serializable