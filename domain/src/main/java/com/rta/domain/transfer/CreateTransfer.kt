package com.rta.domain.transfer

import com.aheaditec.architecture.domain.interactor.BaseUseCase
import com.aheaditec.architecture.domain.interactor.None
import com.aheaditec.functional.Either
import com.rta.domain.account.entity.Account
import com.rta.domain.transfer.entity.Transfer
import java.math.BigDecimal
import javax.inject.Inject

class CreateTransfer @Inject constructor() :
    BaseUseCase<CreateTransfer.Params, Either<CreateTransfer.TransferFailure, Transfer>>() {

    suspend operator fun invoke(
        source: Account,
        counterAccountNumber: Long,
        amount: BigDecimal
    ): Either<TransferFailure, Transfer> = execute(Params(source, counterAccountNumber, amount))

    override suspend fun execute(input: Params): Either<TransferFailure, Transfer> = with(input) {
        if (source.balance - amount < BigDecimal.ZERO) {
            Either.Left(TransferFailure.InsufficientFunds)
        } else {
            Either.Right(toTransfer())
        }
    }

    private fun Params.toTransfer() =
        Transfer(System.currentTimeMillis(), source, counterAccountNumber, source.currency, amount)

    data class Params(val source: Account, val counterAccountNumber: Long, val amount: BigDecimal)

    sealed class TransferFailure {
        object InsufficientFunds : TransferFailure()
    }
}