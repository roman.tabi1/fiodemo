package com.rta.domain.transfer

import com.rta.domain.transfer.entity.Transfer

interface TransferRepository {

    suspend fun getTransfers(): List<Transfer>

    suspend fun saveTransfer(transfer: Transfer)
}