package com.rta.domain.transfer

import com.aheaditec.architecture.domain.interactor.BaseUseCase
import com.aheaditec.architecture.domain.interactor.None
import com.aheaditec.functional.Either
import com.rta.domain.account.entity.Account
import com.rta.domain.transfer.entity.Transfer
import javax.inject.Inject

class GetTransfersForAccount @Inject constructor(private val transferRepository: TransferRepository) :
    BaseUseCase<Account?, List<Transfer>>() {

    override suspend fun execute(input: Account?): List<Transfer> =
        transferRepository
            .takeIf { input != null }
            ?.getTransfers()
            ?.filter {
                it.sourceAccount.number == input?.number || it.counterAccountNumber == input?.number
            }
            ?: emptyList()
}