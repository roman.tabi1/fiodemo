package com.rta.domain.account

import com.aheaditec.functional.Either
import com.rta.domain.account.entity.Account

interface AccountRepository {

    suspend fun getAccounts(timeout: Long = 10_000L): Either<AccountFailure, List<Account>>

    sealed class AccountFailure {
        object NoData : AccountFailure()
        data class DataError(val message: String) : AccountFailure()
        object Timeout : AccountFailure()
        data class Error(val message: String) : AccountFailure()
    }
}