package com.rta.domain.account

import com.aheaditec.architecture.domain.interactor.BaseUseCase
import com.aheaditec.architecture.domain.interactor.None
import com.aheaditec.functional.Either
import com.rta.domain.account.entity.Account
import javax.inject.Inject

class GetAccounts @Inject constructor(private val accountRepository: AccountRepository) :
    BaseUseCase<None, Either<AccountRepository.AccountFailure, List<Account>>>() {

    override suspend fun execute(input: None): Either<AccountRepository.AccountFailure, List<Account>> =
        accountRepository.getAccounts()
}