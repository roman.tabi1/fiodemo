package com.rta.domain.account.entity

import com.rta.domain.serializer.BigDecimalSerializer
import kotlinx.serialization.Serializable
import java.math.BigDecimal

@Serializable
data class Account(
    val name: String,
    val number: Long,
    val currency: String,
    @Serializable(BigDecimalSerializer::class) val balance: BigDecimal,
): java.io.Serializable