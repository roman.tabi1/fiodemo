package com.rta.domain.transfer

import com.aheaditec.functional.Either
import com.aheaditec.functional.withLeft
import com.aheaditec.functional.withRight
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.rta.domain.UnitTest
import com.rta.domain.account.entity.Account
import com.rta.domain.transfer.entity.Transfer
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.amshove.kluent.shouldBe
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import java.math.BigDecimal

class GetTransfersForAccountTest : UnitTest() {

    private lateinit var uut: GetTransfersForAccount

    @Mock
    lateinit var transferRepository: TransferRepository

    @Before
    fun setup() {
        uut = GetTransfersForAccount(transferRepository)
    }

    @Test
    fun `should return transfers related to provided account`(): Unit = runBlockingTest {
        val account1 = mock<Account> { on { number } doReturn 1234L }
        val transfer1 = mock<Transfer> {
            on { counterAccountNumber } doReturn 1111L
            on { sourceAccount } doReturn account1
        }

        val account2 = mock<Account> { on { number } doReturn 1111L }
        val transfer2 = mock<Transfer> {
            on { counterAccountNumber } doReturn 1234L
            on { sourceAccount } doReturn account2
        }

        val account3 = mock<Account> { on { number } doReturn 2222L }
        val transfer3 = mock<Transfer> {
            on { counterAccountNumber } doReturn 3333L
            on { sourceAccount } doReturn account3
        }

        whenever(transferRepository.getTransfers()).thenReturn(
            listOf(
                transfer1,
                transfer3,
                transfer2,
            )
        )

        val input = mock<Account> { on { number } doReturn 1234L }

        val result = uut(input)

        result shouldBeInstanceOf List::class
        result.size shouldBeEqualTo 2
        with(result[0]) {
            sourceAccount.number shouldBeEqualTo 1234L
            counterAccountNumber shouldBeEqualTo 1111L
        }
        with(result[1]) {
            sourceAccount.number shouldBeEqualTo 1111L
            counterAccountNumber shouldBeEqualTo 1234L
        }
    }

    @Test
    fun `should return empty list if provided account is null`(): Unit = runBlockingTest {
        val result = uut(null)

        result shouldBeInstanceOf List::class
        result.size shouldBeEqualTo 0
    }
}