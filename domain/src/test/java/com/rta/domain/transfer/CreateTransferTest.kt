package com.rta.domain.transfer

import com.aheaditec.functional.Either
import com.aheaditec.functional.withLeft
import com.aheaditec.functional.withRight
import com.nhaarman.mockitokotlin2.whenever
import com.rta.domain.UnitTest
import com.rta.domain.account.entity.Account
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.amshove.kluent.shouldBe
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import java.math.BigDecimal

class CreateTransferTest : UnitTest() {

    @Mock
    lateinit var sourceAccount: Account

    private lateinit var uut: CreateTransfer

    @Before
    fun setup() {
        whenever(sourceAccount.currency).thenReturn("CZK")

        uut = CreateTransfer()
    }

    @Test
    fun `should return insufficient funds when amount is bigger than source's balance`(): Unit =
        runBlockingTest {
            whenever(sourceAccount.balance).thenReturn(BigDecimal.ONE)
            val amount = BigDecimal.TEN

            val result = uut(sourceAccount, 0L, amount)

            result shouldBeInstanceOf Either.Left::class
            result.withLeft {
                it shouldBe CreateTransfer.TransferFailure.InsufficientFunds
            }
        }

    @Test
    fun `should return correct transfer when amount is equal to source's balance`(): Unit =
        runBlockingTest {
            whenever(sourceAccount.balance).thenReturn(BigDecimal.TEN)
            val amount = BigDecimal.TEN

            val result = uut(sourceAccount, 0L, amount)

            result shouldBeInstanceOf Either.Right::class
            result.withRight {
                it.amount shouldBeEqualTo BigDecimal.TEN
                it.sourceAccount shouldBe sourceAccount
            }
        }

    @Test
    fun `should return correct transfer when amount is lower than source's balance`(): Unit =
        runBlockingTest {
            whenever(sourceAccount.balance).thenReturn(BigDecimal.TEN)
            val amount = BigDecimal.ONE

            val result = uut(sourceAccount, 0L, amount)

            result shouldBeInstanceOf Either.Right::class
            result.withRight {
                it.amount shouldBeEqualTo BigDecimal.ONE
                it.sourceAccount shouldBe sourceAccount
            }
        }
}